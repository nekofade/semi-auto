#!/bin/bash
sys_runing_dir=$(cd `dirname $0` && pwd)
source ${sys_runing_dir}/../.temp

	echo ''
	echo 'Этап разметки диска был запущен'
	echo ''

case $parted_variant in
	
	1)
		if [[ "$swap_on" -eq "1" ]] 
			then
				parted /dev/${sys_install_dev}l mklabel msdos
				parted /dev/${sys_install_dev} -- mkpart primary 1mib 3mib
				parted /dev/${sys_install_dev} -- mkpart primary 4mib -100s
				mkfs.${parted_fs} /dev/${sys_install_dev}2
				mkdir /mnt/gentoo
				mount /dev/{sys_install_dev}2 /mnt/gentoo
				# - - -
				fallocate -l ${swap_size}mib /mnt/gentoo/swapfile
				chmod 600 /mnt/gentoo/swapfile
				mkswap /mnt/gentoo/swapfile
				swapon /mnt/gentoo/swapfile
		
			else
				parted /dev/${sys_install_dev}l mklabel msdos
				parted /dev/${sys_install_dev} -- mkpart primary 1mib 3mib
				parted /dev/${sys_install_dev} -- mkpart primary 4mib -100s
				mkfs.${parted_fs} /dev/${sys_install_dev}2
				mkdir /mnt/gentoo
				mount /dev/{sys_install_dev}2 /mnt/gentoo
		fi
	;;
	
	2)
		if [[ "$swap_on" -eq "1" ]] 
			then
				parted /dev/${sys_install_dev}l mklabel msdos
				parted /dev/${sys_install_dev} -- mkpart primary 1mib 3mib
				parted /dev/${sys_install_dev} -- mkpart primary 4mib 516mib
				parted /dev/${sys_install_dev} -- mkpart primary 517mib -100s
				mkfs.ext2 /dev/${sys_install_dev}2
				mkfs.${parted_fs} /dev/${sys_install_dev}3
				mkdir /mnt/gentoo
				mkdir /mnt/gentoo/boot
				mount /dev/{sys_install_dev}3 /mnt/gentoo
				mount /dev/{sys_install_dev}2 /mnt/gentoo/boot
				# - - -
				fallocate -l ${swap_size}mib /mnt/gentoo/swapfile
				chmod 600 /mnt/gentoo/swapfile
				mkswap /mnt/gentoo/swapfile
				swapon /mnt/gentoo/swapfile
		
			else
				parted /dev/${sys_install_dev}l mklabel msdos
				parted /dev/${sys_install_dev} -- mkpart primary 1mib 3mib
				parted /dev/${sys_install_dev} -- mkpart primary 4mib 516mib
				parted /dev/${sys_install_dev} -- mkpart primary 517mib -100s
				mkfs.ext2 /dev/${sys_install_dev}2
				mkfs.${parted_fs} /dev/${sys_install_dev}3
				mkdir /mnt/gentoo
				mkdir /mnt/gentoo/boot
				mount /dev/{sys_install_dev}3 /mnt/gentoo
				mount /dev/{sys_install_dev}2 /mnt/gentoo/boot
		fi
	;;
	
	3)
		if [[ "$swap_on" -eq "1" ]] 
			then
				parted /dev/${sys_install_dev}l mklabel msdos
				parted /dev/${sys_install_dev} -- mkpart primary 1mib 3mib
				parted /dev/${sys_install_dev} -- mkpart primary 4mib 516mib
				parted /dev/${sys_install_dev} -- mkpart primary 517mib ${swap_size}mib
				parted /dev/${sys_install_dev} -- mkpart primary ${swap_size_end}mib -100s
				mkfs.ext2 /dev/${sys_install_dev}2
				mkswap /dev/${sys_install_dev}3
				swapon /dev/${sys_install_dev}3
				mkfs.${parted_fs} /dev/${sys_install_dev}4
		
			else
				parted /dev/${sys_install_dev}l mklabel msdos
				parted /dev/${sys_install_dev} -- mkpart primary 1mib 3mib
				parted /dev/${sys_install_dev} -- mkpart primary 4mib 516mib
				parted /dev/${sys_install_dev} -- mkpart primary 517mib ${swap_size}mib
				parted /dev/${sys_install_dev} -- mkpart primary ${swap_size_end}mib -100s
				mkfs.ext2 /dev/${sys_install_dev}2
				mkswap /dev/${sys_install_dev}3
				swapon /dev/${sys_install_dev}3
				mkfs.${parted_fs} /dev/${sys_install_dev}4
		fi
	;;
 		
 	*)
		echo ''
		echo 'Произошла ошибка, перезапустите установщик!'
		echo ''
		sleep 1
esac
echo 'Разметка диска завершена успешно!'
exit 0
