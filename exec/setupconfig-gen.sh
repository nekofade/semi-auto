#!/bin/bash

# Vars
parted_exec_stop="0"
parted_sel_stop="0"
parted_fs_stop="0"
sys_runing_dir=$(cd `dirname $0` && pwd)

#Running
clear
	echo ''
	echo 'Запущен конфигуратор настроек, для последующей установки'
	echo ''

rm ${sys_runing_dir}/../.temp
echo '# Не пытайтесь изменить этот файл руками!' >> ${sys_runing_dir}/../.temp
echo "# Don't try edit this file!'" >> ${sys_runing_dir}/../.temp

lsblk
while [ ${parted_sel_stop} -lt 1 ]
do
	echo 'Укажите три буквы устройства после /dev/, например 'sda vda hda''
	read parted_letter
	parted_letter=${parted_letter,,}
	letter_count=${#parted_letter} 
		echo '' 
	if [[ "$letter_count" -eq "3" ]] 
		then
			ls /dev/${parted_letter}
				if [[ "$?" -eq "0" ]] 
					then
						parted_sel_stop="1"	
						echo "sys_install_dev='${parted_letter}'" >> ${sys_runing_dir}/../.temp				
					else
						echo ''
						echo 'Данный диск не существует!'
						echo ''
			fi
		else 
			echo 'Это не три буквы, попробуйте снова'
			echo ''
	fi
done

# - - -
while [ ${parted_exec_stop} -lt 1 ]
do
	echo ''
	echo 'Укажите тип разметки диска'
	echo '1 - "ALL-IN-ONE"'
	echo 'Будет создан один раздел на жестком диске'
	echo ''
	echo '2 - /boot отдельно'
	echo 'Раздел /boot будет создан отдельно'
	echo ''
	echo '3 - /boot & SWAP отдельно'
	echo 'Разделы /boot и SWAP будут созданы отдельно'
	echo ''
	
# - - - - - - - - - - - - - - - - - - - - - - - - - -
	read parted_install_step
	case $parted_install_step in
		
		1)
				echo ''
				echo 'Выбран вариант: "ALL-IN-ONE"'
				echo "parted_variant='1'" >> ${sys_runing_dir}/../.temp
			sleep 1
			parted_exec_stop="1"
		;;
	
		2)
				echo ''
				echo 'Выбран вариант: "/boot отдельно"'
				echo "parted_variant='2'" >> ${sys_runing_dir}/../.temp
			sleep 1
			parted_exec_stop="1"
		;;
		
		3)
				echo ''
				echo 'Выбран вариант: "/boot & SWAP отдельно"'
				echo "parted_variant='3'" >> ${sys_runing_dir}/../.temp
			sleep 1
			parted_exec_stop="1"
		;;
		
		*)
			echo ''
			echo 'Некоректный ввод!'
			echo ''
			sleep 1

	esac
done

if [[ "$parted_install_step" -eq "3" ]] 
	then
			echo ''
			echo 'Укажите размер SWAP в МЕГАБАЙТАХ'
			read swap_size
			swap_size=${swap_size,,}
			echo "swap_on='1'" >> ${sys_runing_dir}/../.temp
			echo "swap_size='${swap_size}'" >> ${sys_runing_dir}/../.temp
			echo "swap_size_end="$((swap_size+1))"" >> ${sys_runing_dir}/../.temp
			echo ''
	else
			function question_swap() {
					while
							echo -n "$1 [y/N] " &&
							read answer || true &&
							! grep -e '^[YyNn]$' <<< $answer > /dev/null;
						do echo -n "Принимается только y/N! ";
					done
				return $(tr 'YyNn' '0011' <<< $answer)
			}
				if question_swap 'Требуется ли создавать SWAP файл?'; then

					echo ''
					echo 'Укажите размер SWAP в МЕГАБАЙТАХ'
					read swap_size
					swap_size=${swap_size,,}
					echo "swap_on='1'" >> ${sys_runing_dir}/../.temp
					echo "swap_size='${swap_size}'" >> ${sys_runing_dir}/../.temp
					echo "swap_size_end="$((swap_size+1))"" >> ${sys_runing_dir}/../.temp
					echo ''
				else
					echo ''
					echo "swap_on='0'" >> ${sys_runing_dir}/../.temp
					echo ''
				fi
	fi

while [ ${parted_fs_stop} -lt 1 ]
do
	echo ''
	echo 'Укажите тип файловой системы'
	echo '1 - EXT4'
	echo '2 - XFS'
	echo ''

	read parted_install_fs
	case $parted_install_fs in
		
		1)
				echo ''
				echo 'казанная файловая система: "EXT4"'
				echo "parted_fs='ext4'" >> ${sys_runing_dir}/../.temp
			sleep 1
			parted_fs_stop="1"
		;;
	
		2)
				echo ''
				echo 'Указанная файловая система: "XFS"'
				echo "parted_fs='xfs'" >> ${sys_runing_dir}/../.temp
			sleep 1
			parted_fs_stop="1"
		;;
				
		*)
			echo ''
			echo 'Некоректный ввод!'
			echo ''
			sleep 1
	esac
done

echo 'Укажите имя второго пользователя'
read user_two
user_two=${user_two,,}
	echo "user_two='${user_two}'" >> ${sys_runing_dir}/../.temp
echo ''

echo 'Укажите название вашего компьютера'
read host_name
host_name=${host_name,,}
	echo "host_name='${host_name}'" >> ${sys_runing_dir}/../.temp
echo ''

function question_locale() {
		while
				echo -n "$1 [y/N] " &&
				read answer || true &&
				! grep -e '^[YyNn]$' <<< $answer > /dev/null;
			do echo -n "Принимается только y/N! ";
		done
	return $(tr 'YyNn' '0011' <<< $answer)
}
	if question_locale 'Желаете ли вы изменить стандартные локали для системы?'; then

		echo ''
		echo 'Открытие редактора...'
		sleep 1
		cp ${sys_runing_dir}/../config/locale.lang-conf ${sys_runing_dir}/../config/new-locale.lang-conf
		nano ${sys_runing_dir}/../config/new-locale.lang-conf
		echo "locale_set='0'" >> ${sys_runing_dir}/../.temp

	else
		echo ''
		echo 'Использование дефолтных локалей'
		echo "locale_set='1'" >> ${sys_runing_dir}/../.temp
	fi

exit 0
